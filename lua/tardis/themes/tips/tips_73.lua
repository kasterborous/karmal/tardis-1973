local style = {
	style_id = "73",
	style_name = "73",
	font = "GModWorldtip",
	padding = 5,
	offset = 20,
	fr_width = 5,
	colors = {
		normal = {
			text = Color(255, 255, 152, 255), -- old color			text = Color(152, 255, 152, 255),
			background = Color(50, 50, 50, 150),
			frame = Color(255, 255, 255, 100),
		},
		highlighted = {
			text = Color(185, 255, 70),
			background = Color(50, 50, 50, 150),
			frame = Color(255, 255, 255, 100),
		}
	}
}
TARDIS:AddTipStyle(style)
