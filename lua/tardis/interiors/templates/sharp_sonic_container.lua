local T = {
	Interior = {
		Parts = {
			sharpsoniccontainer 		= {pos = Vector(183, -150.083, 38.8500)},
			sharpsoniccontainer_b 		= {pos = Vector(179.5, -150.083, 44), 			ang = Angle(15, 0, 0)},
			sharpsoniccontainer_c 		= {pos = Vector(177, -148.783, 42), 			ang = Angle(-19, -30, -5)},
			sharpsoniccontainer_d 		= {pos = Vector(179.4, -148.983, 41), 			ang = Angle(-12, 0, -8)},
			sharpsoniccontainer_e 		= {pos = Vector(179.5, -149.783, 43), 			ang = Angle(19, -49, -5)},
		},
		Controls = {
			sharpsoniccontainer 		= "sonic_dispenser",
			sharpsoniccontainer_b 		= "sonic_dispenser",
			sharpsoniccontainer_c 		= "sonic_dispenser",
			sharpsoniccontainer_d 		= "sonic_dispenser",
			sharpsoniccontainer_e 		= "sonic_dispenser",
		},
	},
}

TARDIS:AddInteriorTemplate("sharp_sonic_container", T)
