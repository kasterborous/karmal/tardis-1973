local T = {
	Interior = {
		Parts = {
			monsterconsole = {},
			sharpconsole = false,
			monstermicrophone = {},

			sharplargeknob3 = false,
			sharpdomelights2 = false,
			sharpdomelights3 = false,

			sharpemergencybox = { pos = Vector(9.58796, 32.4119, 40.4093), ang = Angle(-19.65, -120, 0) },
		},
		PartTips = {
			sharpemergencybox = { pos = Vector(9.44144,32.3523,40.4356), right = true, }
		},
	},
}

TARDIS:AddInteriorTemplate("time_monster_console", T)
