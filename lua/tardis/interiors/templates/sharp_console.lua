local T = {
	Interior = {
		Parts = {
			sharprotor 					= {pos = Vector(0, 0, -60)},
			sharpconsole 				= {},
			sharpconsolehatch 			= {pos = Vector(-14.2513, 0.000932, 7.9942),		ang = Angle(0, 180, 0)},
			sharpconsolehatchopened 		= {pos = Vector(-14.2513, 0.000932, 7.9942),		ang = Angle(95, 180, 0)},
			sharpconsolelamps 			= {},
			sharpspeakervent 			= {},
			sharptelepathiccircuits 	= {},
			sharpmeters 				= {},
			sharpsmalllights 			= {},
			sharpdomelights1 			= {},
			sharpdomelights2 			= {},
			sharpdomelights3 			= {},
			sharpsmallleversbase 		= {},
			sharptimerammeter 			= {pos = Vector(-32.1466, 25.6436, 38.3035), 	ang = Angle(-19.65, -60, 0)},
			sharpmetersneedle1 			= {pos = Vector(-22.5833, 7.08868, 44.0319), 	ang = Angle(-19.65, 0, 0)},
			sharpmetersneedle2 			= {pos = Vector(-22.5833, -6.98793, 44.0319), 	ang = Angle(-19.65, 0, 0)},
			sharpmetersneedle3 			= {pos = Vector(-32.3597, -11.3101, 40.5443), 	ang = Angle(-19.65, 0, 0)},

			sharpforcefieldgenerator		= {pos = Vector(-4.95128, 0.000932, 9.3007)},
			sharpforcefieldgeneratorswitch		= {pos = Vector(-10.8638, 3.24095, 12.6232),	ang = Angle(-80, 0, 5)},


			sharpemergencybox 			= {pos = Vector(-5.56226, -35.8334, 39.9597), 	ang = Angle(-19.65, 60, 0)},

			sharpdematcircuit 			= {pos = Vector(-18.4538, -16.2514, 38.2996), 	ang = Angle(-19.65, 60, 0)},

			sharplightswitch 			= {pos = Vector(-35.1891, -2.0999, 39.4701), 	ang = Angle(-19.65, 0, 0)},

			sharpknob2 					= {pos = Vector(-5.26081, 22.6093, 44.0526), 	ang = Angle(-19.65, -60, 0)},

			sharpwhiteknob1 			= {pos = Vector(-38.5344, 15.6458, 38.2755), 	ang = Angle(-19.65, 0, 0)},
			sharpwhiteknob3 			= {pos = Vector(4.69629, 22.1349, 44.3514), 	ang = Angle(-19.65, -120, 0)},

			sharpblackknob 				= {pos = Vector(13.9575, 24.1751, 42.1646), 	ang = Angle(-19.65, -120, 0)},

			sharpsilverknob 			= {pos = Vector(16.0627, 27.8205, 40.6566), 	ang = Angle(-19.65, -120, 0)},

			sharprotary 				= {pos = Vector(22.836, 24.5331, 40.4651), 		ang = Angle(-19.65, -120, 0)},

			sharplargeknob1 			= {pos = Vector(-26.4391, -9.24964, 42.5892), 	ang = Angle(-19.65, 0, 0)},
			sharplargeknob3 			= {pos = Vector(9.44144, 32.3523, 40.4356), 	ang = Angle(-19.65, -107, -4.15468)},

			sharpsmalllever1a 			= {pos = Vector(-26.3546, 4.00036, 42.9917), 	ang = Angle(0, 0, 0)},
			sharpsmalllever1b 			= {pos = Vector(-30.8546, 4.00035, 41.3917), 	ang = Angle(0, 0, 0)},
			sharpsmalllever1c 			= {pos = Vector(-35.3546, 4.00035, 39.7917), 	ang = Angle(0, 0, 0)},

			sharphandlelever1a 			= {pos = Vector(-26.4488, 0.100717, 42.5792), 	ang = Angle(-19.65, 0, 0)},
			sharphandlelever1b 			= {pos = Vector(-26.4488, -4.39928, 42.5792), 	ang = Angle(-19.65, 0, 0)},
			sharphandlelever4 			= {pos = Vector(31.1488, 7.69928, 40.8942), 	ang = Angle(-19.65, 180, 0)},
			sharphandlelever6a 			= {pos = Vector(-13.0616, -22.422, 42.7542), 	ang = Angle(-19.65, 60, 0)},
			sharphandlelever6b 			= {pos = Vector(-31.985, -24.1983, 38.8292), 	ang = Angle(-19.65, 60, 0)},

			sharpbiglever1 				= {pos = Vector(33.8008, -0.025371, 39.953), 	ang = Angle(-19.65, 180, 0)},
			sharpbiglever2 				= {pos = Vector(15.6241, -32.6052, 39.1363), 	ang = Angle(-19.65, 120, 0)},
			sharpbiglever3 				= {pos = Vector(10.861, -35.3552, 39.1363), 	ang = Angle(-19.65, 120, 0)},

			sharpblackswitch1 			= {pos = Vector(-7.31515, 40.1247, 38.3748), 	ang = Angle(-19.65, -60, 0)},
			sharpblackswitch2 			= {pos = Vector(-22.0376, 31.6247, 38.3748), 	ang = Angle(-19.65, -60, 0)},
			sharpblackswitch3 			= {pos = Vector(-25.5017, 29.6247, 38.3748), 	ang = Angle(-19.65, -60, 0)},

			sharpblacktoggleswitch 		= {pos = Vector(18.672, 32.3401, 38.7816), 		ang = Angle(0, -90, 0)},

			sharpswitch2a 				= {pos = Vector(-11.2819, 37.8411, 38.4227), 	ang = Angle(0, -30, 0)},
			sharpswitch2b 				= {pos = Vector(-13.014, 36.8411, 38.4227), 	ang = Angle(0, -30, 0)},
			sharpswitch2c 				= {pos = Vector(-16.3179, 34.9336, 38.4227), 	ang = Angle(0, -30, 0)},
			sharpswitch2d 				= {pos = Vector(-17.4437, 34.2836, 38.4227), 	ang = Angle(0, -30, 0)},
			sharpswitch3a 				= {pos = Vector(22.5211, 30.207, 38.8086), 		ang = Angle(0, -90, 0)},
			sharpswitch3b 				= {pos = Vector(14.9001, 34.607, 38.8086), 		ang = Angle(0, -90, 0)},
			sharpswitch3c 				= {pos = Vector(8.31831, 38.407, 38.8086), 		ang = Angle(0, -90, 0)},
			sharpswitch6a 				= {pos = Vector(-25.7957, -27.3749, 39.068), 	ang = Angle(0, 90, 0)},
			sharpswitch6b 				= {pos = Vector(-24.0626, -28.373, 39.0687), 	ang = Angle(0, 90, 0)},
			sharpswitch6c				 = {pos = Vector(-22.3295, -29.3711, 39.0687), 	ang = Angle(0, 90, 0)},
			sharpswitch6d 				= {pos = Vector(-26.5476, -28.6807, 38.5299), 	ang = Angle(0, 90, 0)},
			sharpswitch6e 				= {pos = Vector(-24.8144, -29.6789, 38.5307), 	ang = Angle(0, 90, 0)},
			sharpswitch6f 				= {pos = Vector(-23.0813, -30.677, 38.5314), 	ang = Angle(0, 90, 0)},

			sharpsmalllever4a 			= {pos = Vector(26.4819, -3.50035, 43.3051), 	ang = Angle(0, 180, 0)},
			sharpsmalllever4b 			= {pos = Vector(26.4819, -1.75035, 43.3051), 	ang = Angle(-15, 180, 0)},
			sharpsmalllever4c 			= {pos = Vector(26.4819, -0.000347, 43.3051), 	ang = Angle(0, 180, 0)},
			sharpsmalllever4d 			= {pos = Vector(26.4819, 1.74965, 43.3051), 	ang = Angle(-20, 180, 0)},
			sharpsmalllever4e 			= {pos = Vector(26.4819, 3.49965, 43.3051), 	ang = Angle(0, 180, 0)},
			sharpsmalllever5a 			= {pos = Vector(20.2713, -26.6116, 40.9051), 	ang = Angle(0, 120, 0)},
			sharpsmalllever5b 			= {pos = Vector(21.7868, -25.7366, 40.9051), 	ang = Angle(-15, 120, 0)},
			sharpsmalllever5c 			= {pos = Vector(23.3024, -24.8616, 40.9051), 	ang = Angle(0, 120, 0)},
			sharpsmalllever5d 			= {pos = Vector(24.8179, -23.9866, 40.9051), 	ang = Angle(-15, 120, 0)},
			sharpsmalllever5e 			= {pos = Vector(26.3335, -23.1116, 40.9051), 	ang = Angle(0, 120, 0)},
			sharpsmalllever5f 			= {pos = Vector(27.849, -22.2366, 40.9051), 	ang = Angle(-10, 120, 0)},

			sharpcabinetdoors_hitbox1 	= {pos = Vector(-21.05, 158.094, 59.017), 		ang = Angle(0, 90, 90)},
			sharpcabinetdoors_hitbox2 	= {pos = Vector( 17.05, 158.094, 59.017), 		ang = Angle(0, 90, 90)},

			sharp_pianokeys1			= {},
			sharp_pianokeys2			= {},
			sharp_manualflightscopes	= {},
		},
		Controls = {
			sharpbiglever1 				= "power",
			sharplightswitch			= "power",
			sharpbiglever2 				= "handbrake",
			sharpbiglever3 				= "teleport",
			sharpsmalllever1a			= "teleport",
			sharpemergencybox 			= "engine_release",
			sharpsmalllever1b 			= "flight",
			sharpsmalllever1c 			= "float",
			sharpswitch6d 				= "door",
			sharpswitch6f 				= "doorlock",
			sharplargeknob1 			= "handbrake",
			sharplargeknob3 			= "coordinates",
			sharp_pianokeys1 			= "coordinates",
			sharpwhiteknob3 			= "isomorphic",
			sharpknob2 					= "spin_toggle",
			sharphandlelever1b 			= nil,
			sharphandlelever4 			= "repair",
			sharphandlelever6a 			= "spin_cycle",
			sharpspeakervent 			= "music",
			sharptelepathiccircuits 	= "destination",
			sharpblackknob 				= "toggle_screens",
			sharp_pianokeys2 			= "virtualconsole",
			sharp_manualflightscopes 	= "thirdperson",
			sharpsmalllever4e 			= "redecorate",
			sharpwhiteknob1				= "physlock",
			sharpsmalllever5a 			= "fastreturn",
			sharpblacktoggleswitch		= "hads",
			sharpswitch3c				= "vortex_flight",
			sharpsmalllever5e 			= "vortex_flight",
			sharpdematcircuit			= "sharp_interrupt_teleport",
			sharpblackswitch2			= "cloak",
			sharphandlelever6b			= "door",
		},
		TipSettings = {
			style = "73",
			view_range_max = 55,
			view_range_min = 40,
		},
		PartTips = {
			sharplightswitch			=	{pos = Vector(-35.1891,-2.0999,39.4701), 	down = true,											},
			sharptelepathiccircuits		=	{pos = Vector(-15.26081,25.6093,42.0526), 	down = true,											},
			sharpspeakervent			=	{pos = Vector(-38.8123,-8.99964,38.1589), 	down = true,											},
			sharpswitch6e				=	{pos = Vector(-24.8144,-29.6789,38.5307), 	right = true,					text = "Interior Doors"	},
			sharpswitch6f				=	{pos = Vector(-23.0813,-30.677,38.5314), 	right = true,	down = true,							},
			sharpswitch3c				=	{pos = Vector(8.31831,38.407,38.8086), 		right = true,											},
			sharpsmalllever1a			=	{pos = Vector(-26.3546,4.00036,42.9917), 									text = "Throttle",		},
			sharpsmalllever1b			=	{pos = Vector(-30.8546,4.00035,41.3917), 															},
			sharpsmalllever1c			=	{pos = Vector(-35.3546,4.00035,39.7917), 															},
			sharpemergencybox			=	{pos = Vector(-5.56226,-35.8334,39.9597), 	right = false,	down = false,							},
			sharpbiglever1				=	{pos = Vector(34.1131,-0.025371,40.8277), 	down = true,											},
			sharpbiglever2				=	{pos = Vector(15.8193,-32.9186,40.0046), 	right = true,	down = true,	text = "Handbrake",		},
			sharpbiglever3				=	{pos = Vector(11.0963,-35.7356,39.9768), 	down = true,					text = "Throttle",		},
			sharpswitch6d				=	{pos = Vector(-26.55, -28.687, 39.089), 	right = false,	down = true,							},
			sharplargeknob1 			=	{pos = Vector(-26.4391,-9.24964,42.5892), 	right = true,					text = "Handbrake",		},
			sharplargeknob3				=	{pos = Vector(9.44144,32.3523,40.4356), 	right = true,											},
			sharpwhiteknob3				=	{pos = Vector(4.69629,22.1349,44.3514), 															},
			sharpknob2					=	{pos = Vector(-5.26081,22.6093,44.0526), 	right = true,					text = "Toggle Spin",	},
			sharphandlelever1b			=	{pos = Vector(-26.4488,-4.39928,42.5792), 	right = true,	down = true,							},
			sharphandlelever4			=	{pos = Vector(31.1488,7.69928,40.8942),		right = true,											},
			sharphandlelever6a			=	{pos = Vector(-13.0616,-22.422,42.7542),	right = true,											},
			sharphandlelever6b			=	{pos = Vector(-31.985,-24.1983,38.8292),	down = false,	right = false,							},
			sharpblackknob				=	{pos = Vector(14.042, 24.108, 41.839),		down = false,											},
			sharp_pianokeys1			=	{pos = Vector(31.137, -26.738, 37.41),		down = true,	right = true,							},
			sharpsmalllever4e 			=	{pos = Vector(25.2, 3.51, 43.313),			down = false,	right = true,							},
			sharpwhiteknob1				=	{pos = Vector(-38.78, 15.47, 38.736),		down = true,	right = true,							},
			sharpsmalllever5a			=	{pos = Vector(20.008, -26.26, 40.5),		down = false,	right = false,	text = "Fast Return",	},
			sharpblacktoggleswitch		=	{pos = Vector(18.755, 32.284, 38.487),		down = true,	right = false,							},
			sharpsmalllever5e			=	{pos = Vector(25.834, -22.275, 40.55),		down = false,	right = true,							},
			sharp_manualflightscopes	=	{pos = Vector(36.767, -7.791, 36.389),		down = true,	right = true,							},
			sharpsilverknob				= 	{pos = Vector(16.184, 28.019, 41.098), 																},
			sharpblackswitch2			= 	{pos = Vector(-22.164, 31.744, 38.544), 	down = true,	right = false, 							},
			sharpblackswitch3 			= 	{pos = Vector(-25.5017, 29.6247, 38.3748), 	down = true,	right = true,	text = "Temporal Collision Safeguards",	},
			sharprotary 				= 	{pos = Vector(22.836, 24.5331, 40.4651), 	down = true,					text = "Gyroscopic Stabiliser",	},
		},
		CustomTips = {
			{pos = Vector(-13.448, -37.029, 38.725),	down = true, right = true, part = "sharp_manualflightscopes", },
			{pos = Vector(32.715, 26.238, 37.55),		down = true, right = false, part = "sharp_manualflightscopes", },
		},
	},
}

T.Exterior = {}

T.Interior.TextureSets = {
	["normal_console"] = {
		prefix = "models/karmal/73/",

		{ "sharprotor", 10, "rotor_core_lamps_bulb" },
		{ "sharprotor", 12, "rotor_lights" },
		{ "sharprotor", 15, "rotor_alt" },

		{ "sharpdematcircuit", 3, "demat_circuit_white" },
		{ "sharpdematcircuit", 4, "demat_circuit_green" },
		{ "sharpdematcircuit", 7, "demat_circuit_blue" },

		{ "sharpconsole", 13, "rear_wall_lights" },

		{ "sharpconsolelamps", 1, "console_lamps_flash" },
		{ "sharpconsolelamps", 3, "console_lamps_flash_slow" },
		{ "sharpconsolelamps", 4, "console_lamps" },
	},
	["poweroff_console"] = {
		prefix = "models/karmal/73/",

		{ "sharprotor", 10, "rotor_core_lamps_bulb_off" },
		{ "sharprotor", 12, "rotor_lights_off" },
		{ "sharprotor", 15, "rotor_alt_off" },

		{ "sharpdematcircuit", 3, "demat_circuit_white_off" },
		{ "sharpdematcircuit", 4, "demat_circuit_green_off" },
		{ "sharpdematcircuit", 7, "demat_circuit_blue_off" },

		{ "sharpconsole", 13, "rear_wall_lights_off" },

		{ "sharpconsolelamps", 1, "console_lamps_off" },
		{ "sharpconsolelamps", 3, "console_lamps_off" },
		{ "sharpconsolelamps", 4, "console_lamps_off" },
	},
	["warning_console"] = {
		prefix = "models/karmal/73/",
	},
}

local function random_decide(value, good1, good2)
	if good1 and value == good1 then return true end
	if good2 and value == good2 then return true end
	if value ~= "random" then return false end
	return (math.random(0,1) == 0)
end

T.Interior.CustomHooks = {
	power_textures_console = {
		{
			["PowerToggled"] = true,
			["HealthWarningToggled"] = true,
		},
		function(self)
			local power = self.exterior:GetData("power-state")
			local warning = self.exterior:GetData("health-warning", false)

			if not power then
				self:ApplyTextureSet("poweroff_console")
			elseif warning then
				self:ApplyTextureSet("normal_console")
				self:ApplyTextureSet("warning_console")
			else
				self:ApplyTextureSet("normal_console")
			end

			local meter1 = self:GetPart("sharpmetersneedle1")
			if IsValid(meter1) then meter1:SetOn(not power) end
		end,
	},
	rotor_init = {
		"PostInitialize",
		function(self)
			if CLIENT then return end
			local id = self.metadata.ID

			local part_rotor = self:GetPart("sharprotor")
			local st_rotoralt = TARDIS:GetCustomSetting(id, "rotoralt", self:GetCreator())

			if IsValid(part_rotor) and random_decide(st_rotoralt, true) then
				part_rotor:SetBodygroup(1, 1)
			end
		end,
	}
}

T.Exterior.CustomHooks = {
	power_textures = {
		{
			["ShouldFailDemat"] = true,
			["ShouldFailMat"] = true,
		},
		function(self)
			local dematcircuit = self.interior:GetPart("sharpdematcircuit")
			if IsValid(dematcircuit) and dematcircuit:GetOn() then
				return true
			end
		end,
	},
}

T.CustomSettings = {
	rotoralt = {
		text = "Rotor Type",
		value_type = "list",
		value = "random",
		options = {
			["random"] = "Random",
			[false] = "Default",
			[true] = "Master",
		}
	},
}

TARDIS:AddInteriorTemplate("sharp_console", T)
