-- 1973 TARDIS

local T = {}
T.Base = "base"
T.Name = "1973 TARDIS"
T.ID = "73"

T.Versions = {
	randomize = true,
	allow_custom = true,
	randomize_custom = true,

	main = {
		classic_doors_id = "73cl",
		double_doors_id = "73",
	},
	other = {},
}

T.Interior = {
	Model = "models/karmal/73/interior.mdl",
	Screens = {
		{
			pos = Vector(-117.07, 85.68, 116.03),
			ang = Angle(0, 55, 96.52),
		 	width = 334,
			height = 260,
			visgui_rows = 4,
			power_off_black = false
		}
	},
	ScreensEnabled = false,
	Sounds = {
		Teleport = {
			demat = "karmal/73/demat.wav",
			mat = "karmal/73/mat.wav",
			demat_fail = "karmal/73/demat_fail_int.wav",
			fullflight = "karmal/73/full.wav"

		},
		Power = {
			On = "karmal/73/powerup.wav", -- Power On
			Off = "karmal/73/powerdown.wav" -- Power Off
		},
		FlightLoop = "karmal/73/flight_loop.wav",
		Door = {
			enabled = true,
			open = "karmal/73/dooropen.wav",
			close = "karmal/73/doorclose.wav"
		},
		Lock = "karmal/73/lock.wav"
	},
	ExitDistance = 1000,
	Sequences = "sharp_sequences",
	Parts = {
		sharpceilinglights 			= true,
		sharpcorridors 				= true,
		sharpcorridors2 			= true,
		sharproom2walldecor 		= true,
		sharpentrancedoorframe 		= {pos = Vector(0, -0.5, 0.05)},
		sharpscanner 				= true,
		sharpfurniture 				= true,
		sharpcabinet 				= true,
		sharpcabinetdoors 			= {pos = Vector(-1.8, 161.75, 32.9879)},
		sharpcabinetbed 			= {pos = Vector(37.8, 160.5, 2.10808), 			ang = Angle(0, -90, 0)},
		sharpcabinetbedhitbox 		= {pos = Vector(37.8, 81.5, 2.10808), 			ang = Angle(0, -90, 0)},
		sharpcabinetbedswitch 		= true,
		sharpoxygensupply 			= {pos = Vector(248, -90, 0), 					ang = Angle(0, -45, 0)},
		sharpbigdoors 				= {ang = Angle(0, -90, 0)},

		sharpintdoors1 				= {pos = Vector(433.65, -25.1073, 0.360367), 	ang = Angle(0, -270, 0)},
		sharpintdoors2 				= {pos = Vector(112.91, -549.455, 0.360367), 	ang = Angle(0, 45, 0)},
		sharpintdoors3 				= {pos = Vector(-278.041, -656.83, 0.360367), 	ang = Angle(0, -90, 0)},

		sharpintdoor1 				= {pos = Vector(500.924, 145.7, 0.349995), 		ang = Angle(0, -180, 0)},
		sharpintdoor2 				= {pos = Vector(362.713, -540.538, 0.350037), 	ang = Angle(0, 270, 0)},
		sharpintdoor3 				= {pos = Vector(108.911, -748.474, 0.350002), 	ang = Angle(0, 0, 0)},
		sharpintdoor4 				= {pos = Vector(-101.103, -589.196, 0.350002), 	ang = Angle(0, 180, 0)},

		sharpbowlroundels 			= {},

		door = {
			model = "models/karmal/73/exterior/doors.mdl",
			posoffset = Vector(27.20, 0, -44.75),
			angoffset = Angle(0, 90, 0)
		},
	},
	Controls = {
		sharpsilverknob = "sharp_screen_vortex",
		sharphandlelever6b = "sharp_intdoors",
		-- these only apply to this TARDIS, so it's not in the template
	},
	Portal = {
		pos = Vector(-1.10, -276.75, 40.05),
		ang = Angle(0, 90, 0),
		width = 150,
		height = 150
	},

	Fallback = {
		pos = Vector(0, -196, 3),
		ang = Angle(0, 90, 0),
	},
	IdleSound = {
		{
			path = "karmal/73/interior.wav",
			volume = 1,
		}
	},
	LightOverride = {
		basebrightness = 0.40,
		nopowerbrightness = 0.08
	},
	Light = {
		color = Color(245, 215, 255),
		warncolor = Color(255, 0, 0),
		pos = Vector(0, 0, 185),
		brightness = 1
	},
	Lights = {
		{color = Color(255, 223, 182),	pos = Vector(340,-30,120),	brightness  =  0.25,	nopower = false},
		{color = Color(255, 223, 182),	pos = Vector(540,-30,75),	brightness  =  0.15,	nopower = false},
--		{color = Color(211, 250, 255),	pos = Vector(-210,160,60),	brightness  =  0.2,	nopower = false},
	},

	TipSettings = {
		style = "73",
		view_range_max = 55,
		view_range_min = 40,
	},

}

T.Exterior = {
	Model = "models/karmal/73/exterior/exterior.mdl",
	Mass = 5000,
	DoorAnimationTime = 0.6,
	ScannerOffset = Vector(30, 0, 50),	
	Fallback = {
		pos = Vector(50, 0, 5),
		ang = Angle(0, 0, 0)
	},
	Light = {
		enabled = true,
		pos = Vector(0.5, 2, 107),
		color = Color(218, 198, 191)
	},
	Sounds = {
		Teleport = {
			demat = "karmal/73/demat.wav",
			mat = "karmal/73/mat.wav",
			demat_fail = "karmal/73/demat_fail_ext.wav"
		},
		FlightLoop = "karmal/73/flight_loopext.wav",
		Door = {
			enabled = true,
			open = "karmal/73/dooropen.wav",
			close = "karmal/73/doorclose.wav"
		},
		Lock = "karmal/73/lock.wav"
	},
	Portal = {
		pos = Vector(26, -0.15, 44.75),
		ang = Angle(0, 0, 0),
		width = 48,
		height = 85
	},
	Parts = {
		door = {
			model = "models/karmal/73/exterior/doorsext.mdl", posoffset = Vector(-27.20, 0, -44.75), angoffset = Angle(0, -90, 0), AnimateSpeed = 1
		},
		vortex = {
			model = "models/doctorwho1200/pertwee/1970timevortex.mdl", --"models/atrovis/custom/vortexes/1_vortex.mdl",
			pos = Vector(0, 0, 50),
			ang = Angle(0, 180, 0),
			scale = 10,
		}
	},
	Teleport = {
		SequenceSpeed = 0.460,
		SequenceSpeedFast = 0.542,
		DematSequence = {
			255,
			200,
			150,
			100,
			70,
			50,
			20,
			0
		},
		MatSequence = {
			0,
			20,
			50,
			100,
			150,
			188,
			255
		}
	},
}


T.Interior.TextureSets = {
	["normal"] = {
		prefix = "models/karmal/73/",
		{ "self", 4, "roundel_backing" },
		{ "self", 7, "bluewall" },
		{ "self", 8, "rear_wall_lights" },
		{ "self", 2, "ceiling_central_lamp" },
		{ "self", 16, "ceiling_metal" },

		{ "sharpceilinglights", 1, "ceiling_metal" },

		{ "sharpfurniture", 7, "ceiling_central_lamp" },

		{ "sharpcorridors", 2, "roundel_backing_corridors" },
		{ "sharpcorridors", 4, "rear_wall_lights" },
		{ "sharpcorridors", 6, "marble" },
		{ "sharpcorridors2", 3, "roundel_backing_corridors" },
		{ "sharpcorridors2", 5, "rear_wall_lights" },
		{ "sharproom2walldecor", 2, "colored_lights" },
	},
	["poweroff"] = {
		prefix = "models/karmal/73/",
		{ "self", 4, "roundel_backing_off" },
		{ "self", 7, "bluewall_off" },
		{ "self", 8, "rear_wall_lights_off" },
		{ "self", 2, "ceiling_central_lamp_off" },
		{ "self", 16, "ceiling_metal_off" },

		{ "sharpceilinglights", 1, "ceiling_metal_off" },

		{ "sharpfurniture", 7, "ceiling_central_lamp_off" },

		{ "sharpcorridors", 2, "roundel_backing_corridors_off" },
		{ "sharpcorridors", 4, "rear_wall_lights_off" },
		{ "sharpcorridors", 6, "marble_off" },
		{ "sharpcorridors2", 3, "roundel_backing_corridors_off" },
		{ "sharpcorridors2", 5, "rear_wall_lights_off" },
		{ "sharproom2walldecor", 2, "colored_lights_off" },
	},
	["warning"] = {
		prefix = "models/karmal/73/",
		{ "self", 4, "roundel_backing_warning" },
		{ "self", 7, "bluewall_warning" },
		{ "self", 2, "ceiling_central_lamp_warning" },
		{ "self", 16, "ceiling_metal_warning" },
		{ "sharpcorridors", 2, "roundel_backing_corridors_warning" },
		{ "sharpcorridors2", 3, "roundel_backing_corridors_warning" },
	},
	["screen_normal"] = {
		{ "sharpscanner", 2, "models/karmal/73/scanner_screen" }
	},
	["screen_gui"] = {
		{ "sharpscanner", 2, "models/karmal/73/transparent" }
	},
	["screen_vortex"] = {
		{ "sharpscanner", 2, "models/karmal/73/scanner_screen_vortex"}
	},
	["screen_warning"] = {
		{ "sharpscanner", 2, "models/karmal/73/scanner_screen"}
	},
	["screen_off"] = {
		{ "sharpscanner", 2, "models/karmal/73/scanner_screen" }
	},
}

local function random_decide(value, good1, good2)
	if good1 and value == good1 then return true end
	if good2 and value == good2 then return true end
	if value ~= "random" then return false end
	return (math.random(0,1) == 0)
end

local function UpdateScreen(interior, exterior)
	local power = exterior:GetData("power-state")
	local warning = exterior:GetData("health-warning", false)
	local vortex = exterior:GetData("vortex")
	local show_vortex = exterior:GetData("sharp_show_vortex", true)
	local screens = interior:GetData("screens_on", false)

	local texture_set = "screen_normal"

	if not power then
		texture_set = "screen_off"
	elseif screens then
		texture_set = "screen_gui"
	elseif warning then
		texture_set = "screen_warning"
	elseif vortex and show_vortex then
		texture_set = "screen_vortex"
	end

	interior:ApplyTextureSet(texture_set)
end

T.Interior.CustomHooks = {
	power_textures = {
		{
			["PowerToggled"] = true,
			["HealthWarningToggled"] = true,
		},
		function(self)
			local power = self.exterior:GetData("power-state")
			local warning = self.exterior:GetData("health-warning", false)

			if not power then
				self:ApplyTextureSet("poweroff")
			elseif warning then
				self:ApplyTextureSet("normal")
				self:ApplyTextureSet("warning")
			else
				self:ApplyTextureSet("normal")
			end
		end,
	},

	optional_parts_init = {
		"PostInitialize",
		function(self)
			if CLIENT then return end
			local id = self.metadata.ID

			local st_bowlroundels = TARDIS:GetCustomSetting(id, "bowlroundels", self:GetCreator())

			local part_bowlroundels = self:GetPart("sharpbowlroundels")
			local part_bigdoors = self:GetPart("sharpbigdoors")
			local part_intdoors = self:GetPart("intdoor")
			local part_rotor = self:GetPart("sharprotor")

			if IsValid(part_bowlroundels) and random_decide(st_bowlroundels, "on") then
				part_rotor:SetBodygroup(2, 1)
				if IsValid(part_bigdoors) then part_bigdoors:SetBodygroup(1, 1) end
				if IsValid(part_intdoors) then part_intdoors:SetBodygroup(1, 1) end

			elseif IsValid(part_bowlroundels) then
				part_bowlroundels:SetPos(self:LocalToWorld(Vector(0,0,300)))
				part_bowlroundels:SetVisible(false)
			end
		end,
	},

	scanner_textures = {
		{
			["ScreensToggled"] = true,
		},
		function(self)
			UpdateScreen(self, self.exterior)
		end,
	},
}

T.Exterior.CustomHooks = {
	scanner_textures = {
		{
			["PowerToggled"] = true,
			["HealthWarningToggled"] = true,
			["ScreensToggled"] = true,
			["StopDemat"] = true,
			["MatStart"] = true,
			["sharp_screen_update"] = true,
		},
		function(self)
			UpdateScreen(self.interior, self)
		end,
	},
}

T.CustomSettings = {
	bowlroundels = {
		text = "Bowl Roundels",
		value_type = "list",
		value = "off",
		options = {
			["random"] = "Random",
			["on"] = "Enabled",
			["off"] = "Disabled",
		}
	},
}

T.Templates={
	sharp_console = {},
	sharp_sonic_container = {
		condition = function(id, ply, ent)
			return file.Exists("models/doctorwho1200/sonics/3rddoctor/3rdpersonsonic.mdl", "GAME")
		end,
	},
}
TARDIS:AddInterior(T)