-- 1973 TARDIS

local T = {}
T.Base = "base"
T.Name = "1973 TARDIS"
T.ID = "sharpvortex"

T.IsVersionOf = "73"

T.Interior = {
	Model = "models/karmal/73/interior.mdl",
	Screens = {
		{
			pos = Vector(-117.07, 85.68, 116.03),
			ang = Angle(0, 55, 96.52),
			width = 334,
			height = 260,
			visgui_rows = 4,
			power_off_black = false
		}
	},
	ScreensEnabled = false,
	Sounds = {
		Teleport = {
			demat = "karmal/73/demat.wav",
			mat = "karmal/73/mat.wav",
			demat_fail = "karmal/73/demat_fail_int.wav",
			fullflight = "karmal/73/full.wav"

		},
		Power = {
			On = "karmal/73/powerup.wav", -- Power On
			Off = "karmal/73/powerdown.wav" -- Power Off
		},
		FlightLoop = "karmal/73/flight_loop.wav",
		Door = {
			enabled = true,
			open = "karmal/73/dooropen.wav",
			close = "karmal/73/doorclose.wav"
		},
		Lock = "karmal/73/lock.wav"
	},
	ExitDistance = 1000,
	Sequences = "sharp_sequences",
	Parts = {
		sharprotor 					= {pos = Vector(0, 0, -60)},
		sharpconsole 				= true,
		sharpconsolehatch 			= {pos = Vector(-14.2513, 0.000932, 7.9942),		ang = Angle(0, 180, 0)},
		sharpconsolehatchopened 		= {pos = Vector(-14.2513, 0.000932, 7.9942),		ang = Angle(95, 180, 0)},
		sharpconsolelamps 			= true,
		sharpspeakervent 			= true,
		sharpceilinglights 			= true,
		sharpcorridors 				= true,
		sharpcorridors2 			= true,
		sharproom2walldecor 		= true,
		sharpentrancedoorframe 		= {pos = Vector(0, -0.5, 0.05)},
		sharpscanner 				= true,
		sharpfurniture 				= true,
		sharpcabinet 				= true,
		sharpcabinetdoors 			= {pos = Vector(-1.8, 161.75, 32.9879)},
		sharpcabinetbed 			= {pos = Vector(37.8, 160.5, 2.10808), 			ang = Angle(0, -90, 0)},
		sharpcabinetbedhitbox 		= {pos = Vector(37.8, 81.5, 2.10808), 			ang = Angle(0, -90, 0)},
		sharpcabinetbedswitch 		= true,
		sharpoxygensupply 			= {pos = Vector(248, -90, 0), 					ang = Angle(0, -45, 0)},
		sharpbigdoors 				= {ang = Angle(0, -90, 0)},
		sharptelepathiccircuits 	= true,
		sharpmeters 				= true,
		sharpsmalllights 			= true,
		sharpdomelights1 			= true,
		sharpdomelights2 			= true,
		sharpdomelights3 			= true,
		sharpleversbase 			= true,
		sharptimerammeter 			= {pos = Vector(-32.1466, 25.6436, 38.3035), 	ang = Angle(-19.65, -60, 0)},
		sharpmetersneedle1 			= {pos = Vector(-22.5833, 7.08868, 44.0319), 	ang = Angle(-19.65, 0, 0)},
		sharpmetersneedle2 			= {pos = Vector(-22.5833, -6.98793, 44.0319), 	ang = Angle(-19.65, 0, 0)},
		sharpmetersneedle3 			= {pos = Vector(-32.3597, -11.3101, 40.5443), 	ang = Angle(-19.65, 0, 0)},

		sharpforcefieldgenerator		= {pos = Vector(-4.95128, 0.000932, 9.3007)},
		sharpforcefieldgeneratorswitch		= {pos = Vector(-10.8638, 3.24095, 12.6232),	ang = Angle(-80, 0, 5)},

		sharpsoniccontainer 		= {pos = Vector(183, -150.083, 38.8500)},
		sharpsoniccontainer_b 		= {pos = Vector(179.5, -150.083, 44), 			ang = Angle(15, 0, 0)},
		sharpsoniccontainer_c 		= {pos = Vector(177, -148.783, 42), 			ang = Angle(-19, -30, -5)},
		sharpsoniccontainer_d 		= {pos = Vector(179.4, -148.983, 41), 			ang = Angle(-12, 0, -8)},
		sharpsoniccontainer_e 		= {pos = Vector(179.5, -149.783, 43), 			ang = Angle(19, -49, -5)},

		sharpintdoors1 				= {pos = Vector(433.65, -25.1073, 0.360367), 	ang = Angle(0, -270, 0)},
		sharpintdoors2 				= {pos = Vector(112.91, -549.455, 0.360367), 	ang = Angle(0, 45, 0)},
		sharpintdoors3 				= {pos = Vector(-278.041, -656.83, 0.360367), 	ang = Angle(0, -90, 0)},

		sharpintdoor1 				= {pos = Vector(500.924, 145.7, 0.349995), 		ang = Angle(0, -180, 0)},
		sharpintdoor2 				= {pos = Vector(362.713, -540.538, 0.350037), 	ang = Angle(0, 270, 0)},
		sharpintdoor3 				= {pos = Vector(108.911, -748.474, 0.350002), 	ang = Angle(0, 0, 0)},
		sharpintdoor4 				= {pos = Vector(-101.103, -589.196, 0.350002), 	ang = Angle(0, 180, 0)},

		sharpemergencybox 			= {pos = Vector(-5.56226, -35.8334, 39.9597), 	ang = Angle(-19.65, 60, 0)},

		sharpdematcircuit 			= {pos = Vector(-18.4538, -16.2514, 38.2996), 	ang = Angle(-19.65, 60, 0)},

		sharplightswitch 			= {pos = Vector(-35.1891, -2.0999, 39.4701), 	ang = Angle(-19.65, 0, 0)},

		sharpknob2 					= {pos = Vector(-5.26081, 22.6093, 44.0526), 	ang = Angle(-19.65, -60, 0)},

		sharpwhiteknob1 			= {pos = Vector(-38.5344, 15.6458, 38.2755), 	ang = Angle(-19.65, 0, 0)},
		sharpwhiteknob3 			= {pos = Vector(4.69629, 22.1349, 44.3514), 	ang = Angle(-19.65, -120, 0)},

		sharpblackknob 				= {pos = Vector(13.9575, 24.1751, 42.1646), 	ang = Angle(-19.65, -120, 0)},

		sharpsilverknob 			= {pos = Vector(16.0627, 27.8205, 40.6566), 	ang = Angle(-19.65, -120, 0)},

		sharprotary 				= {pos = Vector(22.836, 24.5331, 40.4651), 		ang = Angle(-19.65, -120, 0)},

		sharplargeknob1 			= {pos = Vector(-26.4391, -9.24964, 42.5892), 	ang = Angle(-19.65, 0, 0)},
		sharplargeknob3 			= {pos = Vector(9.44144, 32.3523, 40.4356), 	ang = Angle(-19.65, -107, -4.15468)},

		sharpsmalllever1a 			= {pos = Vector(-26.3546, 4.00036, 42.9917), 	ang = Angle(0, 0, 0)},
		sharpsmalllever1b 			= {pos = Vector(-30.8546, 4.00035, 41.3917), 	ang = Angle(0, 0, 0)},
		sharpsmalllever1c 			= {pos = Vector(-35.3546, 4.00035, 39.7917), 	ang = Angle(0, 0, 0)},

		sharphandlelever1a 			= {pos = Vector(-26.4488, 0.100717, 42.5792), 	ang = Angle(-19.65, 0, 0)},
		sharphandlelever1b 			= {pos = Vector(-26.4488, -4.39928, 42.5792), 	ang = Angle(-19.65, 0, 0)},
		sharphandlelever4 			= {pos = Vector(31.1488, 7.69928, 40.8942), 	ang = Angle(-19.65, 180, 0)},
		sharphandlelever6a 			= {pos = Vector(-13.0616, -22.422, 42.7542), 	ang = Angle(-19.65, 60, 0)},
		sharphandlelever6b 			= {pos = Vector(-31.985, -24.1983, 38.8292), 	ang = Angle(-19.65, 60, 0)},

		sharpbiglever1 				= {pos = Vector(34.1131, -0.025371, 40.8277), 	ang = Angle(19.65, 0, 0)},
		sharpbiglever2 				= {pos = Vector(15.8193, -32.9186, 40.0046), 	ang = Angle(19.65, -60, 0)},
		sharpbiglever3 				= {pos = Vector(11.0963, -35.7356, 39.9768), 	ang = Angle(19.65, -60, 0)},

		sharpblackswitch1 			= {pos = Vector(-7.31515, 40.1247, 38.3748), 	ang = Angle(-19.65, -60, 0)},
		sharpblackswitch2 			= {pos = Vector(-22.0376, 31.6247, 38.3748), 	ang = Angle(-19.65, -60, 0)},
		sharpblackswitch3 			= {pos = Vector(-25.5017, 29.6247, 38.3748), 	ang = Angle(-19.65, -60, 0)},

		sharpblacktoggleswitch 		= {pos = Vector(18.672, 32.3401, 38.7816), 		ang = Angle(0, -90, 0)},

		sharpswitch2a 				= {pos = Vector(-11.2819, 37.8411, 38.4227), 	ang = Angle(0, -30, 0)},
		sharpswitch2b 				= {pos = Vector(-13.014, 36.8411, 38.4227), 	ang = Angle(0, -30, 0)},
		sharpswitch2c 				= {pos = Vector(-16.3179, 34.9336, 38.4227), 	ang = Angle(0, -30, 0)},
		sharpswitch2d 				= {pos = Vector(-17.4437, 34.2836, 38.4227), 	ang = Angle(0, -30, 0)},
		sharpswitch3a 				= {pos = Vector(22.5211, 30.207, 38.8086), 		ang = Angle(0, -90, 0)},
		sharpswitch3b 				= {pos = Vector(14.9001, 34.607, 38.8086), 		ang = Angle(0, -90, 0)},
		sharpswitch3c 				= {pos = Vector(8.31831, 38.407, 38.8086), 		ang = Angle(0, -90, 0)},
		sharpswitch6a 				= {pos = Vector(-25.7957, -27.3749, 39.068), 	ang = Angle(0, 90, 0)},
		sharpswitch6b 				= {pos = Vector(-24.0626, -28.373, 39.0687), 	ang = Angle(0, 90, 0)},
		sharpswitch6c				 = {pos = Vector(-22.3295, -29.3711, 39.0687), 	ang = Angle(0, 90, 0)},
		sharpswitch6d 				= {pos = Vector(-26.5476, -28.6807, 38.5299), 	ang = Angle(0, 90, 0)},
		sharpswitch6e 				= {pos = Vector(-24.8144, -29.6789, 38.5307), 	ang = Angle(0, 90, 0)},
		sharpswitch6f 				= {pos = Vector(-23.0813, -30.677, 38.5314), 	ang = Angle(0, 90, 0)},

		sharpsmalllever4a 			= {pos = Vector(26.4819, -3.50035, 43.3051), 	ang = Angle(0, 180, 0)},
		sharpsmalllever4b 			= {pos = Vector(26.4819, -1.75035, 43.3051), 	ang = Angle(-15, 180, 0)},
		sharpsmalllever4c 			= {pos = Vector(26.4819, -0.000347, 43.3051), 	ang = Angle(0, 180, 0)},
		sharpsmalllever4d 			= {pos = Vector(26.4819, 1.74965, 43.3051), 	ang = Angle(-20, 180, 0)},
		sharpsmalllever4e 			= {pos = Vector(26.4819, 3.49965, 43.3051), 	ang = Angle(0, 180, 0)},
		sharpsmalllever5a 			= {pos = Vector(20.2713, -26.6116, 40.9051), 	ang = Angle(0, 120, 0)},
		sharpsmalllever5b 			= {pos = Vector(21.7868, -25.7366, 40.9051), 	ang = Angle(-15, 120, 0)},
		sharpsmalllever5c 			= {pos = Vector(23.3024, -24.8616, 40.9051), 	ang = Angle(0, 120, 0)},
		sharpsmalllever5d 			= {pos = Vector(24.8179, -23.9866, 40.9051), 	ang = Angle(-15, 120, 0)},
		sharpsmalllever5e 			= {pos = Vector(26.3335, -23.1116, 40.9051), 	ang = Angle(0, 120, 0)},
		sharpsmalllever5f 			= {pos = Vector(27.849, -22.2366, 40.9051), 	ang = Angle(-10, 120, 0)},

		sharpcabinetdoors_hitbox1 	= {pos = Vector(-21.05, 158.094, 59.017), 		ang = Angle(0, 90, 90)},
		sharpcabinetdoors_hitbox2 	= {pos = Vector( 17.05, 158.094, 59.017), 		ang = Angle(0, 90, 90)},

		sharp_pianokeys1			= {},
		sharp_pianokeys2			= {},
		sharp_manualflightscopes	= {},

		sharpbowlroundels 				= {},

		door = {
			model = "models/karmal/73/exterior/doors.mdl",
			posoffset = Vector(27.20, 0, -44.75),
			angoffset = Angle(0, 90, 0)
		},
	},
	Portal = {
		pos = Vector(-1.10, -276.75, 40.05),
		ang = Angle(0, 90, 0),
		width = 150,
		height = 150
	},

	Fallback = {
		pos = Vector(0, -200, 3),
		ang = Angle(0, 90, 0),
	},
	Controls = {
		sharpbiglever1 				= "power",
		sharplightswitch			= "power",
		sharpbiglever2 				= "handbrake",
		sharpbiglever3 				= "teleport",
		sharpsmalllever1a			= "teleport",
		sharpemergencybox 			= "engine_release",
		sharpsmalllever1b 			= "flight",
		sharpsmalllever1c 			= "float",
		sharpswitch6d 				= "door",
		sharpswitch6e 				= "sharp_intdoors",
		sharpswitch6f 				= "doorlock",
		sharplargeknob1 			= "handbrake",
		sharplargeknob3 			= "coordinates",
		sharp_pianokeys1 			= "coordinates",
		sharpwhiteknob3 			= "isomorphic",
		sharpknob2 					= "spin_toggle",
		sharphandlelever1b 			= nil,
		sharphandlelever4 			= "repair",
		sharphandlelever6a 			= "spin_cycle",
		sharphandlelever6b 			= "cloak",
		sharpspeakervent 			= "music",
		sharptelepathiccircuits 	= "destination",
		sharpblackknob 				= "toggle_screens",
		sharp_pianokeys2 			= "virtualconsole",
		sharp_manualflightscopes 	= "thirdperson",
		sharpsmalllever4e 			= "redecorate",
		sharpwhiteknob1				= "physlock",
		sharpsmalllever5a 			= "fastreturn",
		sharpblacktoggleswitch		= "hads",
		sharpswitch3c				= "vortex_flight",
		sharpsmalllever5e 			= "vortex_flight",
		sharpdematcircuit			= "sharp_interrupt_teleport",

	},
	IdleSound = {
		{
			path = "karmal/73/interior.wav",
			volume = 1	
		}
	},
	LightOverride = {
		basebrightness = 0.40,
		nopowerbrightness = 0.08
	},
	Light = {
		color = Color(245, 215, 255),
		warncolor = Color(255, 0, 0),
		pos = Vector(0, 0, 185),
		brightness = 1
	},
	Lights = {
		{color = Color(255, 223, 182),	pos = Vector(340,-30,120),	brightness  =  0.25,	nopower = false},
		{color = Color(255, 223, 182),	pos = Vector(540,-30,75),	brightness  =  0.15,	nopower = false},
--		{color = Color(211, 250, 255),	pos = Vector(-210,160,60),	brightness  =  0.2,	nopower = false},
	},

	TipSettings = {
		style = "73",
		view_range_max = 60,
		view_range_min = 45,
	},
	PartTips = {
		sharplightswitch			=	{pos = Vector(-35.1891,-2.0999,39.4701), 	down = true},
		sharptelepathiccircuits		=	{pos = Vector(-15.26081,25.6093,42.0526), 	down = true,},
		sharpspeakervent			=	{pos = Vector(-38.8123,-8.99964,38.1589), 	down = true},
		sharpswitch6e				=	{pos = Vector(-24.8144,-29.6789,38.5307), 	right = true},
		sharpswitch6f				=	{pos = Vector(-23.0813,-30.677,38.5314), 	right = true, down = true},
		sharpswitch3c				=	{pos = Vector(8.31831,38.407,38.8086), 		right = true},
		sharpsmalllever1a			=	{pos = Vector(-26.3546,4.00036,42.9917), 	text = "Throttle", },
		sharpsmalllever1b			=	{pos = Vector(-30.8546,4.00035,41.3917), 	},
		sharpsmalllever1c			=	{pos = Vector(-35.3546,4.00035,39.7917), 	},
		sharpemergencybox			=	{pos = Vector(-5.56226,-35.8334,39.9597), 	right = false, down = false},
		sharpbiglever1				=	{pos = Vector(34.1131,-0.025371,40.8277), 	down = true},
		sharpbiglever2				=	{pos = Vector(15.8193,-32.9186,40.0046), 	right = true, down = true, text = "Handbrake",},
		sharpbiglever3				=	{pos = Vector(11.0963,-35.7356,39.9768), 	down = true, text = "Throttle",},
		sharpswitch6d				=	{pos = Vector(-26.55, -28.687, 39.089), 	right = false, down = true,},
		sharplargeknob1 			=	{pos = Vector(-26.4391,-9.24964,42.5892), 	right = true, text = "Handbrake",},
		sharplargeknob3				=	{pos = Vector(9.44144,32.3523,40.4356), 	right = true},
		sharpwhiteknob3				=	{pos = Vector(4.69629,22.1349,44.3514), 	},
		sharpknob2					=	{pos = Vector(-5.26081,22.6093,44.0526), 	right = true, text = "Toggle Spin"},
--		sharphandlelever1b			=	{pos = Vector(-26.4488,-4.39928,42.5792), 	right = true, down = true},
		sharphandlelever4			=	{pos = Vector(31.1488,7.69928,40.8942),		right = true},
		sharphandlelever6a			=	{pos = Vector(-13.0616,-22.422,42.7542),	right = true},
		sharphandlelever6b			=	{pos = Vector(-31.985,-24.1983,38.8292),	down = false, right = true,},
		sharpblackknob				=	{pos = Vector(14.042, 24.108, 41.839),		down = false},
		sharp_pianokeys1			=	{pos = Vector(31.137, -26.738, 37.41),		down = true, right = true,},
		sharpsmalllever4e 			=	{pos = Vector(25.2, 3.51, 43.313),			down = false, right = true,},
		sharpwhiteknob1				=	{pos = Vector(-38.78, 15.47, 38.736),		down = true, right = false,},
		sharpsmalllever5a			=	{pos = Vector(20.008, -26.26, 40.5),		down = false, right = false, text = "Fast Return"},
		sharpblacktoggleswitch		=	{pos = Vector(18.755, 32.284, 38.487),		down = true, right = false, },
		sharpsmalllever5e			=	{pos = Vector(25.834, -22.275, 40.55),		down = false, right = true, },
		sharp_manualflightscopes	=	{pos = Vector(36.767, -7.791, 36.389),	down = true, right = true, },
		sharpblackswitch3 		= 	{pos = Vector(-25.5017, 29.6247, 38.3748), 	down = true, text = "Temporal Collision Safeguards"},
		sharprotary 				= {pos = Vector(22.836, 24.5331, 40.4651), 		text = "Gyroscopic Stabiliser"},

	},
	CustomTips = {
		{pos = Vector(-13.448, -37.029, 38.725),	down = true, right = true, part = "sharp_manualflightscopes", },
		{pos = Vector(32.715, 26.238, 37.55),		down = true, right = false, part = "sharp_manualflightscopes", },
	},

}

T.Exterior = {
	Model = "models/karmal/73/exterior/exterior.mdl",
	Mass = 5000,
	DoorAnimationTime = 0.6,
	ScannerOffset = Vector(30, 0, 50),	
	Fallback = {
		pos = Vector(50, 0, 5),
		ang = Angle(0, 0, 0)
	},
	Light = {
		enabled = true,
		pos = Vector(0.5, 2, 107),
		color = Color(255, 228, 91)
	},
	Sounds = {
		Teleport = {
			demat = "karmal/73/demat.wav",
			mat = "karmal/73/mat.wav",
			demat_fail = "karmal/73/demat_fail_ext.wav"
		},
		FlightLoop = "karmal/73/flight_loopext.wav",
		Door = {
			enabled = true,
			open = "karmal/73/dooropen.wav",
			close = "karmal/73/doorclose.wav"
		},
		Lock = "karmal/73/lock.wav"
	},
	Portal = {
		pos = Vector(26, -0.15, 44.75),
		ang = Angle(0, 0, 0),
		width = 48,
		height = 85
	},
	Parts = {
		door = {
			model = "models/karmal/73/exterior/doorsext.mdl", posoffset = Vector(-27.20, 0, -44.75), angoffset = Angle(0, -90, 0), AnimateSpeed = 1
		},
		vortex = {
			model = "models/atrovis/custom/vortexes/2_vortex.mdl",
			pos = Vector(0, 0, 50),
			ang = Angle(0, 180, 0),
			scale = 10,
		}
	},
	Teleport = {
		SequenceSpeed = 0.460,
		SequenceSpeedFast = 0.542,
		DematSequence = {
			255,
			200,
			150,
			100,
			70,
			50,
			20,
			0
		},
		MatSequence = {
			0,
			20,
			50,
			100,
			150,
			188,
			255
		}
	},
	CustomHooks = {
		power_textures = {
			{
				["ShouldFailDemat"] = true,
				["ShouldFailMat"] = true,
			},
			function(self)
				local dematcircuit = self.interior:GetPart("sharpdematcircuit")
				if IsValid(dematcircuit) and dematcircuit:GetOn() then
					return true
				end
			end,
		},
	},
}


T.Interior.TextureSets = {
	["normal"] = {
		prefix = "models/karmal/73/",
		{ "self", 4, "roundel_backing" },
		{ "self", 7, "bluewall" },
		{ "self", 8, "rear_wall_lights" },
		{ "self", 2, "ceiling_central_lamp" },

		{ "sharprotor", 14, "rotor_lights" },
		{ "sharprotor", 9, "rotor_core_lamps_bulb" },

		{ "sharpdematcircuit", 3, "demat_circuit_white" },
		{ "sharpdematcircuit", 4, "demat_circuit_green" },
		{ "sharpdematcircuit", 7, "demat_circuit_blue" },

		{ "sharpfurniture", 7, "ceiling_central_lamp" },

		{ "sharpcorridors", 2, "roundel_backing_corridors" },
		{ "sharpcorridors", 4, "rear_wall_lights" },
		{ "sharpcorridors", 6, "marble" },
		{ "sharpcorridors2", 3, "roundel_backing_corridors" },
		{ "sharpcorridors2", 5, "rear_wall_lights" },
		{ "sharproom2walldecor", 2, "colored_lights" },

		{ "sharpconsole", 13, "rear_wall_lights" },

		{ "sharpconsolelamps", 1, "console_lamps_flash" },
		{ "sharpconsolelamps", 3, "console_lamps_flash_slow" },
		{ "sharpconsolelamps", 4, "console_lamps" },
	},
	["poweroff"] = {
		prefix = "models/karmal/73/",
		{ "self", 4, "roundel_backing_off" },
		{ "self", 7, "bluewall_off" },
		{ "self", 8, "rear_wall_lights_off" },
		{ "self", 2, "ceiling_central_lamp_off" },

		{ "sharprotor", 14, "rotor_lights_off" },
		{ "sharprotor", 9, "rotor_core_lamps_bulb_off" },

		{ "sharpdematcircuit", 3, "demat_circuit_white_off" },
		{ "sharpdematcircuit", 4, "demat_circuit_green_off" },
		{ "sharpdematcircuit", 7, "demat_circuit_blue_off" },

		{ "sharpfurniture", 7, "ceiling_central_lamp_off" },

		{ "sharpcorridors", 2, "roundel_backing_corridors_off" },
		{ "sharpcorridors", 4, "rear_wall_lights_off" },
		{ "sharpcorridors", 6, "marble_off" },
		{ "sharpcorridors2", 3, "roundel_backing_corridors_off" },
		{ "sharpcorridors2", 5, "rear_wall_lights_off" },
		{ "sharproom2walldecor", 2, "colored_lights_off" },

		{ "sharpconsole", 13, "rear_wall_lights_off" },

		{ "sharpconsolelamps", 1, "console_lamps_off" },
		{ "sharpconsolelamps", 3, "console_lamps_off" },
		{ "sharpconsolelamps", 4, "console_lamps_off" },
	},
	["warning"] = {
		prefix = "models/karmal/73/",
		{ "self", 4, "roundel_backing_warning" },
		{ "self", 7, "bluewall_warning" },
		{ "self", 2, "ceiling_central_lamp_warning" },
		{ "sharpcorridors", 2, "roundel_backing_corridors_warning" },
		{ "sharpcorridors2", 3, "roundel_backing_corridors_warning" },
	},
}

local function random_decide(value, good1, good2)
	if good1 and value == good1 then return true end
	if good2 and value == good2 then return true end
	if value ~= "random" then return false end
	return (math.random(0,1) == 0)
end

T.Interior.CustomHooks = {
	power_textures = {
		{
			["PowerToggled"] = true,
			["HealthWarningToggled"] = true,
		},
		function(self)
			local power = self.exterior:GetData("power-state")
			local warning = self.exterior:GetData("health-warning", false)

			if not power then
				self:ApplyTextureSet("poweroff")
			elseif warning then
				self:ApplyTextureSet("normal")
				self:ApplyTextureSet("warning")
			else
				self:ApplyTextureSet("normal")
			end

			local meter1 = self:GetPart("sharpmetersneedle1")
			if IsValid(meter1) then meter1:SetOn(not power) end
		end,
	},



	optional_parts_init = {
		"PostInitialize",
		function(self)
			local id = self.metadata.ID

			local st_bowlroundels = TARDIS:GetCustomSetting(id, "bowlroundels", self:GetCreator())

			local part_bowlroundels = self:GetPart("sharpbowlroundels")

			if SERVER and IsValid(part_bowlroundels) and random_decide(st_bowlroundels, "off") then
				part_bowlroundels:SetPos(self:LocalToWorld(Vector(0,0,300)))
			end
		end,
	},

}

T.CustomSettings = {
	bowlroundels = {
		text = "Bowl Roundels",
		value_type = "list",
		value = "off",
		options = {
			["random"] = "Select random",
			["on"] = " Enabled",
			["off"] = " Disabled",
		}
	},
}

TARDIS:AddInterior(T)