-- 1973 TARDIS (classic doors)

local door_offset = 26.70 --26.70

local T={}
T.Base="73"
T.Name="1973 TARDIS"
T.ID="73cl"
T.EnableClassicDoors = true

T.IsVersionOf = "73"

T.Interior={
	Portal = {
		pos = Vector(-1.10, -166.15, 41.05),
		ang = Angle(0, 90, 0),
		width = 150,
		height = 200
	},
	Fallback = {
		pos = Vector(0, -141.5, 3),
		ang = Angle(0, 90, 0),
	},
	Sounds={
		Door={
			enabled=true,
			open = "karmal/73/bigdoors.wav",
			close = "karmal/73/bigdoors.wav",
		},
	},
	Parts={
		sharpbigdoors=false,
		intdoor = {
			model="models/karmal/73/bigdoors.mdl",
			ang = Angle(0, -90, 0),
		},
		door = {
			posoffset = Vector(door_offset, 1.25, -44.75),
		},
	},
	Controls = {
		sharphandlelever6b = "door",
	},
	IntDoorAnimationTime = 3,
}

T.Exterior = {
	Portal = {
		pos = Vector(25.5, 1, 44.75),
		ang = Angle(0, 0, 0),
		width = 40,
		height = 85
	},
	Parts = {
		door = {
			posoffset = Vector(-door_offset, -1.25, -44.75),
		},
	},
}

TARDIS:AddInterior(T)