--1973 TARDIS interior - Control sequences (advanced mode)

local Seq = {
	ID = "sharp_sequences",

	["sharplargeknob3"] = {
		Controls = {
			"sharprotary",
			"sharphandlelever4",
			"sharpsmalllever5e",
			"sharpbiglever2",
			"sharplargeknob1",
			"sharpsmalllever1c",
			"sharpsmalllever1b",
			"sharpsmalllever1a",
		},
		OnFinish = function(self, ply, step, part)
			if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end,
		OnFail = function(self, ply, step, part)
			-- fail stuff
		end,
		Condition = function(self)
			return self.exterior:GetData("vortex",false)==false and self.exterior:GetData("teleport",false)==false
		end,
	},

	["sharp_pianokeys1"] = {
		Controls = {
			"sharpsmalllever5a",
			"sharprotary",
			"sharpknob2",
			"sharpwhiteknob1",
			"sharphandlelever6a",
			"sharpbiglever2",
			"sharpbiglever3",
		},
		OnFinish = function(self, ply, step, part)
			if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end,
		OnFail = function(self, ply, step, part)
			-- fail stuff
		end,
		Condition = function(self)
			return self.exterior:GetData("vortex",false)==false and self.exterior:GetData("teleport",false)==false
		end,
	},

	["sharptelepathiccircuits"] = {
		Controls = {
			"sharpblackknob",
			"sharpbiglever1",
			"sharphandlelever6b",
			"sharplargeknob1",
			"sharpsmalllever1c",
			"sharpsmalllever1b",
			"sharpsmalllever1a",
		},
		OnFinish = function(self, ply, step, part)
			if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end,
		OnFail = function(self, ply, step, part)
			-- fail stuff
		end,
		Condition = function(self)
			return self.exterior:GetData("vortex",false)==false and self.exterior:GetData("teleport",false)==false
		end,
	}
}

TARDIS:AddControlSequence(Seq)