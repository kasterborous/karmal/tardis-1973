TARDIS:AddControl({
	id = "sharp_interrupt_teleport",
	tip_text = "Interrupt Teleport",
	serveronly=true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply)
		local dematcircuit = TARDIS:GetPart(self.interior, "sharpdematcircuit")
		if not IsValid(dematcircuit) then return end

		if not dematcircuit:GetOn() and self:GetData("teleport") then
			self:InterruptTeleport()
			self:TogglePower()
		end
	end,
})