TARDIS:AddControl({
	id = "sharp_intdoors",
	tip_text = "Main doors",
	serveronly=true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local intdoors = TARDIS:GetPart(self, "sharpbigdoors")
		if not IsValid(intdoors) then return end
		intdoors:Toggle(!intdoors:GetOn(), ply)
	end,
})