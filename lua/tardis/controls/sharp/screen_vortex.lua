TARDIS:AddControl({
	id = "sharp_screen_vortex",
	tip_text = "Vortex scanner",
	serveronly=true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply)
		local show = not self:GetData("sharp_show_vortex", true)
		self:SetData("sharp_show_vortex", show, true)
		self:CallHook("sharp_screen_update")
	end,
})