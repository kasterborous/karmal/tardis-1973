local PART={}
PART.ID = "sharpsmalllever4a"
PART.Name = "sharpsmalllever4a"
PART.Model = "models/karmal/73/small_lever_red.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/73/smalllever.wav"

TARDIS:AddPart(PART)