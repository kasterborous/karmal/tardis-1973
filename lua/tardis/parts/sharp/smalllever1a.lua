local PART={}
PART.ID = "sharpsmalllever1a"
PART.Name = "sharpsmalllever1a"
PART.Model = "models/karmal/73/small_lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/73/smalllever.wav"

TARDIS:AddPart(PART)