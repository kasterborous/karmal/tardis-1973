local PART={}
PART.ID = "sharpemergencybox"
PART.Name = "sharpemergencybox"
PART.Model = "models/karmal/73/extreme_emergency_box.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.25
PART.SoundOn = "karmal/73/extreme_emergency_box_open.wav"
PART.SoundOff = "karmal/73/extreme_emergency_box_close.wav"

if SERVER then
	function PART:Use(ply)
		if self:GetOn() then
			TARDIS:Control(self.Control, ply)
		end
	end
end

TARDIS:AddPart(PART)