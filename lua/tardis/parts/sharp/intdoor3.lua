local PART={}
PART.ID = "sharpintdoor3"
PART.Name = "1973 TARDIS Small Interior Door 3"
PART.Model = "models/karmal/73/intdoor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = false
PART.AnimateSpeed = 1
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)