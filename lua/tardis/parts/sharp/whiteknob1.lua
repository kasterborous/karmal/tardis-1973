local PART={}
PART.ID = "sharpwhiteknob1"
PART.Name = "sharpwhiteknob1"
PART.Model = "models/karmal/73/white_knob_1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/knob_spin_small.wav"

TARDIS:AddPart(PART)