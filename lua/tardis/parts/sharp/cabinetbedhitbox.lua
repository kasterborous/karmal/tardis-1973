local PART={}
PART.ID = "sharpcabinetbedhitbox"
PART.Name = "sharpcabinetbedhitbox"
PART.Model = "models/karmal/73/cabinet_bed.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = false
PART.Sound = "karmal/73/cabinet_bed.wav"

if SERVER then
	function PART:Initialize()
		self:SetCollide(false, true)
		self:SetVisible(false)
	end

	function PART:Use(a)
		local bed = self.interior:GetPart("sharpcabinetbed")
		bed:SetOn(not bed:GetOn())
		bed:SetCollide(not bed:GetOn(), true)
		self:SetCollide(bed:GetOn(), true)
	end
end
TARDIS:AddPart(PART)