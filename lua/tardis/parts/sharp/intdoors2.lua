local PART={}
PART.ID = "sharpintdoors2"
PART.Name = "1973 TARDIS Interior Doors 2"
PART.Model = "models/karmal/73/intdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.ShouldTakeDamage = true

PART.SoundOff = "karmal/73/intdoors_close.wav"
PART.SoundOn = "karmal/73/intdoors_open.wav"

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end
end

TARDIS:AddPart(PART)