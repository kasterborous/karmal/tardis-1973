local PART={}
PART.ID = "sharpceilinglights"
PART.Name = "1973 TARDIS Ceiling Lights"
PART.Model = "models/karmal/73/ceiling_lights.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.ShouldTakeDamage = false

if CLIENT then
	function PART:Think()
		local exterior=self.exterior
		local power=self.exterior:GetData("power-state")

		if power == true then
			if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") then
				self:SetSubMaterial(2 , "models/karmal/73/ceiling_lights_lamp_flight")
			else
				self:SetSubMaterial(2 , "models/karmal/73/ceiling_lights_lamp")
			end
		else
			self:SetSubMaterial(2 , "models/karmal/73/ceiling_lights_lamp_off")
		end
	end
end


TARDIS:AddPart(PART)