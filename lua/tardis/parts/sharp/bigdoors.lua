local PART={}
PART.ID = "sharpbigdoors"
PART.Name = "1973 TARDIS Big Doors"
PART.Model = "models/karmal/73/bigdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.30
PART.ShouldTakeDamage = true

PART.Sound = "karmal/73/bigdoors.wav"
PART.SoundPos = Vector(-1.10, -166.15, 41.05)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
	end
end

TARDIS:AddPart(PART)