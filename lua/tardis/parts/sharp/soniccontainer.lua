local PART={}
PART.ID = "sharpsoniccontainer"
PART.Name = "1973 TARDIS Sonic Container"
PART.Model = "models/karmal/73/sonic_container.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = false

TARDIS:AddPart(PART)