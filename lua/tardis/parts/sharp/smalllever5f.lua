local PART={}
PART.ID = "sharpsmalllever5f"
PART.Name = "sharpsmalllever5f"
PART.Model = "models/karmal/73/small_lever_red.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/73/smalllever.wav"

TARDIS:AddPart(PART)