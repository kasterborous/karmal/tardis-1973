local PART={}
PART.ID = "sharpcabinetbed"
PART.Name = "1973 TARDIS Cabinet Bed"
PART.Model = "models/karmal/73/cabinet_bed.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.3
PART.ShouldTakeDamage = true
PART.Sound = "karmal/73/cabinet_bed.wav"

if SERVER then
	function PART:Use()
		local bedhitbox = self.interior:GetPart("sharpcabinetbedhitbox")
		self:SetCollide(self:GetOn(), true)
		bedhitbox:SetCollide(not self:GetOn(), true)
	end
end


TARDIS:AddPart(PART)