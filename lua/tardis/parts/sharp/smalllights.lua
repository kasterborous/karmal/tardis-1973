local PART={}
PART.ID = "sharpsmalllights"
PART.Name = "1973 TARDIS Small Lights"
PART.Model = "models/karmal/73/small_lights.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true
//PART.Collision = true

if CLIENT then
	function PART:Think()
		local power=self.exterior:GetData("power-state")
		local switch = TARDIS:GetPart(self.interior,"sharpswitch3c")
		if power == true then
			if ( switch:GetOn() ) then
				self:SetSubMaterial(1 , "models/karmal/73/colored_lights_off")
			else
				self:SetSubMaterial(1 , "models/karmal/73/colored_lights")
			end
		else
			self:SetSubMaterial(1 , "models/karmal/73/colored_lights_off")
		end
	end
end

TARDIS:AddPart(PART)