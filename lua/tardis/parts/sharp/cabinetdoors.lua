local PART={}
PART.ID = "sharpcabinetdoors"
PART.Name = "1973 TARDIS Cabinet Doors"
PART.Model = "models/karmal/73/cabinet_doors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.ShouldTakeDamage = true

PART.Sound = "karmal/73/interiordooropen.wav"

local function update_hitboxes(self, on)
	local hitbox1 = self.interior:GetPart("sharpcabinetdoors_hitbox1")
	local hitbox2 = self.interior:GetPart("sharpcabinetdoors_hitbox2")

	if IsValid(hitbox1) then hitbox1:SetCollide(on, true) end
	if IsValid(hitbox2) then hitbox2:SetCollide(on, true) end
end

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn(), true)
		update_hitboxes(self, not self:GetOn())
	end
end

TARDIS:AddPart(PART)

PART.ID = "sharpcabinetdoors_hitbox1"
PART.Name = "1973 TARDIS Cabinet Doors Hitbox 1"
PART.Model = "models/hunter/plates/plate05x075.mdl"
PART.Animate = false
PART.Sound = "karmal/73/interiordoorclose.wav"

if SERVER then
	function PART:Initialize()
		self:SetCollide(false, true)
		self:SetVisible(false)
	end

	function PART:Use(a)
		local cabinet_doors = self.interior:GetPart("sharpcabinetdoors")
		if not IsValid(cabinet_doors) then return end

		cabinet_doors:SetOn(not cabinet_doors:GetOn())
		cabinet_doors:SetCollide(not cabinet_doors:GetOn(), true)
		update_hitboxes(self, cabinet_doors:GetOn())
	end
end
TARDIS:AddPart(PART)

PART.ID = "sharpcabinetdoors_hitbox2"
PART.Name = "1973 TARDIS Cabinet Doors Hitbox 2"
TARDIS:AddPart(PART)