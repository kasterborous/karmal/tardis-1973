-- Adds 1973 TARDIS Rotor

local PART={}
PART.ID = "sharprotor"
PART.Name = "1973 TARDIS Rotor"
PART.Model = "models/karmal/73/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.ShouldTakeDamage = true

if CLIENT then

	function PART:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	end

	function PART:Think()

		local exterior=self.exterior
		local interior=self.interior
		local console=interior:GetPart("console")
		local rotor=interior:GetPart("sharprotor")

		local power = exterior:GetData("power-state")
		local flight = exterior:GetData("flight")
		local teleport = exterior:GetData("teleport")
		local vortex = exterior:GetData("vortex")
		local float = exterior:GetData("float")
		local active = (flight or teleport or vortex)

		if active or self.timerotor.pos > 0 then
			if self.timerotor.pos == 0 then
				self.timerotor.pos = 1
			elseif active and self.timerotor.pos == 1 then
				self.timerotor.pos=0
			end

			self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.3 )
			self:SetPoseParameter( "motion", self.timerotor.pos )
		end

		if power == true then
			if active or float then
				rotor:SetSubMaterial(12 , "models/karmal/73/rotor_lights_flight")
				rotor:SetSubMaterial(15 , "models/karmal/73/rotor_alt_flight")
			else
				rotor:SetSubMaterial(12 , "models/karmal/73/rotor_lights_off")
				rotor:SetSubMaterial(15 , "models/karmal/73/rotor_alt")
			end
		else
			rotor:SetSubMaterial (15 , "models/karmal/73/rotor_alt_off")
		end
	end
end

TARDIS:AddPart(PART)