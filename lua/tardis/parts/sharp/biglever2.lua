local PART={}
PART.ID = "sharpbiglever2"
PART.Name = "sharpbiglever2"
PART.Model = "models/karmal/73/big_lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "karmal/73/big_lever.wav"

TARDIS:AddPart(PART)