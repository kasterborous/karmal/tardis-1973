local PART={}
PART.ID = "sharpsmalllever4d"
PART.Name = "sharpsmalllever4d"
PART.Model = "models/karmal/73/small_lever_green.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/73/smalllever.wav"

TARDIS:AddPart(PART)