local PART={}
PART.ID = "sharpdomelights2"
PART.Name = "1973 TARDIS Dome Lights 2"
PART.Model = "models/karmal/73/dome_lights_2.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false
//PART.Collision = true

if CLIENT then
	function PART:Think()
		local power=self.exterior:GetData("power-state")
		local switch = TARDIS:GetPart(self.interior,"sharpemergencybox")
		if power == true then
			if ( switch:GetOn() ) then
				self:SetSubMaterial(1 , "models/karmal/73/colored_lights")
			else
				self:SetSubMaterial(1 , "models/karmal/73/colored_lights_off")
			end
		else
			self:SetSubMaterial(1 , "models/karmal/73/colored_lights_off")
		end
	end
end


TARDIS:AddPart(PART)