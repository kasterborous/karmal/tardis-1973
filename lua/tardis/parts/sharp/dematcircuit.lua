local PART={}
PART.ID = "sharpdematcircuit"
PART.Name = "sharpdematcircuit"
PART.Model = "models/karmal/73/demat_circuit_housing.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.75
PART.SoundOn = "karmal/73/demat_circuit_housing_open.wav"
PART.SoundOff = "karmal/73/demat_circuit_housing_close.wav"

TARDIS:AddPart(PART)