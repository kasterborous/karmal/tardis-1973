local PART={}
PART.ID = "sharpswitch2c"
PART.Name = "sharpswitch2c"
PART.Model = "models/karmal/73/toggle_switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/73/toggle_switch.wav"

TARDIS:AddPart(PART)