local PART={}
PART.ID = "sharpfurniture"
PART.Name = "1973 TARDIS Furniture"
PART.Model = "models/karmal/73/furniture.mdl"
PART.AutoSetup = true
PART.UseTransparencyFix = true
PART.Collision = true
PART.ShouldTakeDamage = false


TARDIS:AddPart(PART)