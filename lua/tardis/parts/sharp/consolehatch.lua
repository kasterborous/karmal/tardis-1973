local PART={}
PART.ID = "sharpconsolehatch"
PART.Name = "1973 TARDIS Console Hatch"
PART.Model = "models/karmal/73/console_hatch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.35
PART.ShouldTakeDamage = true

PART.Sound = "karmal/73/console_hatch_open.wav"

local function update_hitboxes(self, on)
	local hitbox = self.interior:GetPart("sharpconsolehatchopened")

	if IsValid(hitbox) then hitbox:SetCollide(on, true) end
end

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn(), true)
		update_hitboxes(self, not self:GetOn())
	end
end

TARDIS:AddPart(PART)

PART.ID = "sharpconsolehatchopened"
PART.Name = "1973 TARDIS Console Hatch Hitbox"
PART.Model = "models/karmal/73/console_hatch.mdl"
PART.Animate = false
PART.Sound = "karmal/73/console_hatch_close.wav"

if SERVER then
	function PART:Initialize()
		self:SetCollide(false, true)
		self:SetVisible(false)
	end

	function PART:Use(a)
		local console_hatch = self.interior:GetPart("sharpconsolehatch")
		if not IsValid(console_hatch) then return end

		console_hatch:SetOn(not console_hatch:GetOn())
		console_hatch:SetCollide(not console_hatch:GetOn(), true)
		update_hitboxes(self, console_hatch:GetOn())
	end
end
TARDIS:AddPart(PART)