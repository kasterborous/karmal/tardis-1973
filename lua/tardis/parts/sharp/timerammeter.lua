local PART={}
PART.ID = "sharptimerammeter"
PART.Name = "sharptimerammeter"
PART.Model = "models/karmal/73/time_ram_meter.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 0.035

if CLIENT then
	function PART:Think()
		local vortex=self.exterior:GetData("vortex")
		local switch = TARDIS:GetPart(self.interior,"sharpblackswitch3")
		if vortex == true then
			if ( switch:GetOn() ) then
				self:SetOn( true )
			else
				self:SetOn( false )
			end

		else
			self:SetOn( false )
		end
	end
end

TARDIS:AddPart(PART)