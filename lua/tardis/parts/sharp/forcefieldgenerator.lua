local PART={}
PART.ID = "sharpforcefieldgenerator"
PART.Name = "1973 TARDIS Forcefield Generator"
PART.Model = "models/karmal/73/forcefield_generator.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = false

if CLIENT then
	function PART:Think()
		local power = self.exterior:GetData("power-state")
		local switch = TARDIS:GetPart(self.interior,"sharpforcefieldgeneratorswitch")
		if power == true then
			if ( switch:GetOn() ) then
				self:SetSubMaterial(1 , "models/karmal/73/colored_lights_off")
			else
				self:SetSubMaterial(1 , "models/karmal/73/colored_lights")
			end
		else
			self:SetSubMaterial(1 , "models/karmal/73/colored_lights_off")
		end
	end
end


TARDIS:AddPart(PART)

local PART={}
PART.ID = "sharpforcefieldgeneratorswitch"
PART.Name = "sharpforcefieldgeneratorswitch"
PART.Model = "models/karmal/73/toggle_switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/73/toggle_switch.wav"

TARDIS:AddPart(PART)