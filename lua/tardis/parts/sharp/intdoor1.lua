local PART={}
PART.ID = "sharpintdoor1"
PART.Name = "1973 TARDIS Small Interior Door 1"
PART.Model = "models/karmal/73/intdoor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.ShouldTakeDamage = true

PART.SoundOff = "karmal/73/intdoor_close.wav"
PART.SoundOn = "karmal/73/intdoor_open.wav"

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end
end

TARDIS:AddPart(PART)