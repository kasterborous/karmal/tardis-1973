local PART={}
PART.ID = "sharpwhiteknob3"
PART.Name = "sharpwhiteknob3"
PART.Model = "models/karmal/73/white_knob_3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/knob_spin_small.wav"

TARDIS:AddPart(PART)