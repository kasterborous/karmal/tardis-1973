local PART={}
PART.ID = "sharptelepathiccircuits"
PART.Name = "1973 TARDIS Telepathic Circuits"
PART.Model = "models/karmal/73/telepathic_circuits.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true
PART.Collision = true


if SERVER then
	function PART:Think()
		local exterior=self.exterior
		local interior=self.interior
		local telepathic=interior:GetPart("sharptelepathiccircuits")

		local flight = exterior:GetData("flight")
		local teleport = exterior:GetData("teleport")
		local vortex = exterior:GetData("vortex")
		local float = exterior:GetData("float")

		if flight or teleport or vortex or float then
			telepathic:SetSubMaterial(2 , "models/karmal/73/telepathic_circuit_lens_flight")
		else
			telepathic:SetSubMaterial(2 , "models/karmal/73/telepathic_circuit_lens")
		end
	end
end


TARDIS:AddPart(PART)