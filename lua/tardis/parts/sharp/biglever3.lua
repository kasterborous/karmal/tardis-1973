local PART={}
PART.ID = "sharpbiglever3"
PART.Name = "sharpbiglever3"
PART.Model = "models/karmal/73/big_lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "karmal/73/big_lever.wav"

if SERVER then
	function PART:Think()
        self:SetSkin(1)
	end
end

TARDIS:AddPart(PART)