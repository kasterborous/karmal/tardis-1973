local PART={}
PART.ID = "sharpblackswitch1"
PART.Name = "sharpblackswitch1"
PART.Model = "models/karmal/73/black_switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "karmal/73/black_switch.wav"

TARDIS:AddPart(PART)