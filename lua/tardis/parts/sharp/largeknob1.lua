local PART={}
PART.ID = "sharplargeknob1"
PART.Name = "sharplargeknob1"
PART.Model = "models/karmal/73/large_knob_1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/knob_spin.wav"

TARDIS:AddPart(PART)