local PART={}
PART.ID = "sharp_manualflightscopes"
PART.Name = "sharp_manualflightscopes"
PART.Model = "models/karmal/73/lights.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = false
PART.Sound = "karmal/73/ball.wav"

if SERVER then
	function PART:Use(ply)
		self.interior:Timer("", 0.4, function()
			TARDIS:Control(self.Control, ply)
		end)
	end
end

TARDIS:AddPart(PART)