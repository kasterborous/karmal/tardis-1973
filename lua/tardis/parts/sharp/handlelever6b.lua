local PART={}
PART.ID = "sharphandlelever6b"
PART.Name = "sharphandlelever6b"
PART.Model = "models/karmal/73/handle_lever_3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/handle_lever.wav"

TARDIS:AddPart(PART)