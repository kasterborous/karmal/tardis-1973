local PART={}
PART.ID = "sharpknob2"
PART.Name = "sharpknob2"
PART.Model = "models/karmal/73/knob_2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/knob_spin_small.wav"

TARDIS:AddPart(PART)