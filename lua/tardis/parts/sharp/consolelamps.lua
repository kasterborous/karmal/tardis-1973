local PART={}
PART.ID = "sharpconsolelamps"
PART.Name = "1973 TARDIS Console Lamps"
PART.Model = "models/karmal/73/console_lamps.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false
PART.Collision = true

TARDIS:AddPart(PART)