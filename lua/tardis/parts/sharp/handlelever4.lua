local PART={}
PART.ID = "sharphandlelever4"
PART.Name = "sharphandlelever4"
PART.Model = "models/karmal/73/handle_lever_1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/handle_lever.wav"

TARDIS:AddPart(PART)