local PART={}
PART.ID = "sharpscanner"
PART.Name = "1973 TARDIS Scanner"
PART.Model = "models/karmal/73/scanner.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = false

TARDIS:AddPart(PART)