local PART={}
PART.ID = "sharprotary"
PART.Name = "sharprotary"
PART.Model = "models/karmal/73/rotary.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/rotary_spin.wav"

TARDIS:AddPart(PART)