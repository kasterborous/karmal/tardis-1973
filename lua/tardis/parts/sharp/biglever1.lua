local PART={}
PART.ID = "sharpbiglever1"
PART.Name = "sharpbiglever1"
PART.Model = "models/karmal/73/big_lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "karmal/73/big_lever.wav"

TARDIS:AddPart(PART)