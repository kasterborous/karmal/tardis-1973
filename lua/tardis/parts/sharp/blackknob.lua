local PART={}
PART.ID = "sharpblackknob"
PART.Name = "sharpblackknob"
PART.Model = "models/karmal/73/black_knob_3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "karmal/73/black_switch.wav"

TARDIS:AddPart(PART)