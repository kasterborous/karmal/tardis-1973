local PART={}
PART.ID = "monstermicrophone"
PART.Name = "1972 TARDIS Microphone"
PART.Model = "models/karmal/72/microphone.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false
PART.Collision = true

TARDIS:AddPart(PART)