local PART={}
PART.ID = "sharpintdoors3"
PART.Name = "1973 TARDIS Interior Doors"
PART.Model = "models/karmal/73/intdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = false
PART.AnimateSpeed = 1
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)