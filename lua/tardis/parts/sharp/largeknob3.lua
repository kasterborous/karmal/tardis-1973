local PART={}
PART.ID = "sharplargeknob3"
PART.Name = "sharplargeknob3"
PART.Model = "models/karmal/73/large_knob_3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/knob_spin.wav"

if SERVER then
	function PART:Use(ply)
		self.interior:Timer("", 0.3, function()
			TARDIS:Control(self.Control, ply)
		end)
	end
end

TARDIS:AddPart(PART)