local PART={}
PART.ID = "sharpblacktoggleswitch"
PART.Name = "sharpblacktoggleswitch"
PART.Model = "models/karmal/73/black_toggle_switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "karmal/73/toggle_switch.wav"

TARDIS:AddPart(PART)