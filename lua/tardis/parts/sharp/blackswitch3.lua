local PART={}
PART.ID = "sharpblackswitch3"
PART.Name = "sharpblackswitch3"
PART.Model = "models/karmal/73/black_switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "karmal/73/black_switch.wav"

TARDIS:AddPart(PART)