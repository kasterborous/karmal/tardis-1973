local PART={}
PART.ID = "sharpmetersneedle2"
PART.Name = "sharpmetersneedle2"
PART.Model = "models/karmal/73/meters_needle_1.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 0.118

if CLIENT then
	function PART:Think()
		local demat=self.exterior:GetData("demat")
		local vortex=self.exterior:GetData("vortex")
		if demat or vortex then
			self:SetOn( false )
		else
			self:SetOn( true )
		end
	end
end

TARDIS:AddPart(PART)