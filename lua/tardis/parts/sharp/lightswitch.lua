local PART={}
PART.ID = "sharplightswitch"
PART.Name = "sharplightswitch"
PART.Model = "models/karmal/73/light_switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/73/lightswitch.wav"

TARDIS:AddPart(PART)