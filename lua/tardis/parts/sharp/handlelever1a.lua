local PART={}
PART.ID = "sharphandlelever1a"
PART.Name = "sharphandlelever1a"
PART.Model = "models/karmal/73/handle_lever_2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/73/handle_lever.wav"

TARDIS:AddPart(PART)