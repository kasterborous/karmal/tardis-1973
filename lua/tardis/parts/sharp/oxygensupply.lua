local PART={}
PART.ID = "sharpoxygensupply"
PART.Name = "1973 TARDIS Oxygen Supply"
PART.Model = "models/karmal/73/oxygen_supply.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.28
PART.ShouldTakeDamage = true
PART.SoundOn = "karmal/73/oxygen_supply_open.wav"
PART.SoundOff = "karmal/73/oxygen_supply_close.wav"


TARDIS:AddPart(PART)