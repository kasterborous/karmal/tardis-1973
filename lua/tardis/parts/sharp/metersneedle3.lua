local PART={}
PART.ID = "sharpmetersneedle3"
PART.Name = "sharpmetersneedle3"
PART.Model = "models/karmal/73/meters_needle_3.mdl"
PART.AutoSetup = true
PART.Animate = true

if CLIENT then

	function PART:Initialize()
		self.needle3={}
		self.needle3.pos=0
		self.needle3.mode=1
	end

	function PART:Think()
		local exterior=self.exterior
		local interior=self.interior
		local power=self.exterior:GetData("power-state")

		if self.needle3.pos > 0 or power == true then
			if self.needle3.pos==0 then
				self.needle3.pos=1
			elseif self.needle3.pos==1 and power == true then
				self.needle3.pos=0
			end

			self.needle3.pos=math.Approach( self.needle3.pos, self.needle3.mode, FrameTime()*0.1 )
			self:SetPoseParameter( "switch", self.needle3.pos )
		end
	end
end

TARDIS:AddPart(PART)