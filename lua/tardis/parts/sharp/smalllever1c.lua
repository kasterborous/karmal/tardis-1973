local PART={}
PART.ID = "sharpsmalllever1c"
PART.Name = "sharpsmalllever1c"
PART.Model = "models/karmal/73/small_lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/73/smalllever.wav"

TARDIS:AddPart(PART)