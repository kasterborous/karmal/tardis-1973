local PART={}
PART.ID = "sharpsilverknob"
PART.Name = "sharpsilverknob"
PART.Model = "models/karmal/73/silver_knob.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "karmal/73/silver_knob.wav"

TARDIS:AddPart(PART)